import { useState, PropsWithChildren } from 'react'
import axios from 'axios';

export default function Input(){

    const [url, setUrl] = useState('');
    const [jsonResponce, setJsonResponce] = useState('');
    const [isError, setIsError] = useState(false);

    const buttonClick = async () => {
        try{
            const response = await axios(url);
            setJsonResponce(response.data);

            if (isError)
                setIsError(false);
        }
        catch (error){
            if(error instanceof Error)
            {
                setJsonResponce(error.message);
                setIsError(true);
            }
        }
        
    }
    
    return <div>
        <input type ="text" value = {url} onChange = {e => setUrl(e.target.value)}></input>
        <br/>
        <button onClick={buttonClick}>Жми</button>
        <br/>
        <div style={{color: isError ? 'red' : 'black' }}>
            <pre>{ JSON.stringify(jsonResponce) }</pre>
        </div>
    </div>
};