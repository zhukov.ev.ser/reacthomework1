import { Component, PropsWithChildren } from 'react'
import axios from 'axios';

interface StateUrl{
    url : string;
    jsonResponce: any | null;
    isError : boolean;
}

export default class Input extends Component<{}, StateUrl>{

    constructor(props:{})
    {
        super(props);
        this.state = {url : "", jsonResponce : null, isError : false};
    }

    changeState(newValue: string){
        this.setState({url: newValue});
    }

    buttonClick = async () => {
        try{
            const response = await axios(this.state.url);
            this.setState({ jsonResponce: response.data });
            
            if (this.state.isError)
                this.setState({ isError : false})
        }
        catch (error){
            if(error instanceof Error)
            {
                this.setState({ jsonResponce: error.message });
                this.setState({ isError : true});
            }
        }
    }

    render(){ 
        return <div>
            <input type ="text" value = {this.state.url} onChange = {e => this.changeState(e.target.value)}></input>
            <br/>
            <button onClick={this.buttonClick}>Жми</button>
            <br/>
            <div>
                <pre style={{color: this.state.isError ? 'red' : 'black' }}>
                    {JSON.stringify(this.state.jsonResponce)}
                </pre>
            </div>
        </div>
    }
}