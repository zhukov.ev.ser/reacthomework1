import React from 'react';
import logo from './logo.svg';
import './App.css';
import InputClass from './components/inputClass';
import InputFunc from './components/inputFunction';


function App() {
  return (
    <div className="App">
      <InputClass/>
      <br/>
      <InputFunc/>
    </div>
  );
}

export default App;
